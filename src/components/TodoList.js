import { useState } from "react";
import TodoListItems from "./TodoListItems";
import InputTask from "./InputTask";
import '../style/TodoListStyle.css'

// todo list print and functions to store data
const TodoList = () => {

    const [tasks , setTasks] = useState([{   
        id:1,
        title:"Temp" ,
        description :"This is demo description",
        isCompleted:false,
        date: Date.now()
    }])


    const addTask = (task)=>{
        setTasks([...tasks,task])
    }
    const deleteTask = (id) => {
        const tempTasks=tasks.filter(tsk => tsk.id!=id)
        setTasks(tempTasks);
    }
    const updateTask = (newTask) =>{
        const updatedTasks = tasks.map(task => (task.id === newTask.id ? newTask : task));
        setTasks(updatedTasks);
    }

    const [openAddTask,setopenAddTask]= useState(false);

    const  handleAddTask =()=>{
        setopenAddTask(!openAddTask)
    }

    return ( 
        <div className="todo-list">
            <h1>Todo List App</h1>

            <button onClick={handleAddTask}>Add Task</button>

            {openAddTask && <InputTask addTask={addTask} closeAddTask={handleAddTask}/>}

            <TodoListItems tasks={tasks} markComplete={updateTask} deleteTask={deleteTask}></TodoListItems>
        </div>
     );
}
 
export default TodoList;
