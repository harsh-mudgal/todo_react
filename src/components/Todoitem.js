const TodoItem = (prop) => {
    return ( 
        <div className="todo-item">
            <h3>{prop.task.title} ({prop.task.date})</h3>
            <p>{prop.task.description}</p>
        </div>
     );
}
 
export default TodoItem;