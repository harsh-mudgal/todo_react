// add task form
import '../style/InputFormStyle.css'
import { useState } from "react";

const InputTask = ({addTask,closeAddTask}) => {

    const [title,setTitle]=useState("")
    const [description,setDescription]=useState("")
    const [date,setDate]=useState("")
    const [errors, setErrors] = useState({
        title: '',
        description: '',
        date: ''
      });

    const isValidDateFormat = (inputDate) => {
        var date_regex = /^\d{2}-\d{2}-\d{4}$/;
        return date_regex.test(inputDate);
    };

    const inputValidation=()=>{
        const errorsCopy = { ...errors };
        if (title.trim().length < 3) {
        errorsCopy.title = 'Min Length is 3';
        } else {
        errorsCopy.title = '';
        }

        if (description.trim().length < 5) {
        errorsCopy.description = 'Min Length is 5';
        } else {
        errorsCopy.description = '';
        }

        if (!isValidDateFormat(date.trim())) {
        errorsCopy.date = 'Date is required';
        } else {
        errorsCopy.date = '';
        }
        return errorsCopy
    }

    const onSubmit=(e)=>{
        e.preventDefault()
        const errorsCopy=inputValidation()

        if (!errorsCopy.title && !errorsCopy.description && !errorsCopy.date) {
            const newTask={   
                id:Date.now(),
                title:title ,
                description :description,
                isCompleted:false,
                date: date
            }
            addTask(newTask);
            closeAddTask();
          } 
          else {
            setErrors(errorsCopy);
          }
    }

    return (  
        <div className="input-task">
            <h2>Add New Task</h2>

            <form onSubmit={onSubmit}>
                <label>Title</label>
                <input placeholder="Title" type="text" required value={title} onChange={(e)=>setTitle(e.target.value)}></input>
                <span>{errors.title}</span>


                <label>Description</label>
                <textarea placeholder="Description" type="text" required value={description} onChange={(e)=>setDescription(e.target.value)}></textarea>
                <span>{errors.description}</span>


                <label>Date</label>
                <input placeholder="Date" type="text" required value={date} onChange={(e)=>setDate(e.target.value)}></input>
                <span>{errors.date}</span>


                <button >Enter</button>

            </form>
        </div>
    );
}
 
export default InputTask;