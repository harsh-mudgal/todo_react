import TodoItem from "./Todoitem";

const TodoListItems = (props) => {
    const tasks=props.tasks
    return ( 
        <div className="todo-list-items">
            <h2>All Tasks</h2>
            {
                tasks.map(task => <TodoItem task={task} markComplete={props.markComplete} deleteTask={props.delete}/>)
            }
        </div>
     );
}
 
export default TodoListItems;