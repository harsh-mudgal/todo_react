import TodoList from './components/TodoList';
import logo from './logo.svg';

function App() {
  return (
    <div className="App">
       <TodoList/>
    </div>
  );
}

export default App;
